<?php

namespace App\Http\Controllers\Auth\Admin;

use App\Models\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Notifications\VerifyRegistration;
use Illuminate\Http\Request;
use File;
use Intervention\Image\Facades\Image as Image;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    public $image;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }



    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm(){
         return view('backend.auth.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:50'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'image' => ['nullable', 'image'],
            'phone' => ['nullable', 'numeric'],
            'role' => ['string', 'required'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Http\Controllers\Auth\Admin\
     */
    protected function register(Request $request){
        $request->validate([
            'name' => 'required|max:255|string',
            'username' => 'required|max:255|string',
            'email' => 'required|email|string|unique:users',
            'password' => 'required|min:8|string|confirmed',
            'role' => 'required|string',
            'phone' => 'nullable|numeric',
            'image' => 'nullable|image',
        ]);
        if ($request->image >0){
            $image = $request->file('image');
            $image_name = 'neershop'.'-'.'admin'.$request->name.'.'.$image->getClientOriginalExtension();
            $location = public_path('images/admin/'.$image_name);
            Image::make($image)->save($location);
            $this->image = $image_name;
        }
        $admin = Admin::create([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'image' => $this->image,
            'phone' => $request->phone,
            'role' => $request->role,
            'password' => Hash::make($request['password']),
            'remember_token' => str_random(50),
            'status' => 0,
            'slug' => str_slug($request->username),


        ]);

        $admin->notify(new VerifyRegistration($admin,$admin->remember_token));

        session()->flash('success','A confirmation message has sent to the new admins Email');
        return redirect()->route('admin.list');
    }
}
