<?php

namespace App\Http\Controllers\Backend;

use Intervention\Image\Facades\Image as Image;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(){
        $admins = Admin::orderBy('name','asc')->get();
        return view('backend.auth.index',compact('admins'));
    }

    public function show($id){
        $admin = Admin::find($id);
        return view('backend.auth.show',compact('admin'));
    }

    public function edit($id){
        $admin = Admin::find($id);
        return view('backend.auth.edit',compact('admin'));
    }

    public function update(Request $request,$id){
        $request->validate([
            'name' => 'required|max:255|string',
            'username' => 'required|max:255|string',
            'phone' => 'nullable|numeric',
            'image' => 'nullable|image|mimes:jpeg,jpg,png',
        ]);
        $admin = Admin::find($id);
        $admin->name     = $request->name;
        $admin->username = $request->username;
        $admin->phone    = $request->phone;
        $admin->slug     = str_slug($request->username);

        //        delete old image
        if (File::exists('images/admin/'.$admin->image)){
            File::delete('images/admin/'.$admin->image);
        }
        //insert image
        if ($request->image > 0){
            $image = $request->file('image');
            $image_name = 'neershop'.'-'.$admin->name.'.'.$image->getClientOriginalExtension();
            $location = public_path('images/admin/'.$image_name);
            Image::make($image)->save($location);
            $admin->image = $image_name;
        }
        $admin->save();
        session()->flash('success','Profile Has Updated');
        return redirect()->route('admin.show',$admin->id);
    }

    public function block($id){
        $admin = Admin::find($id);
        $admin->status =2;
        $admin->save();

        session()->flash('success','Blocked Done');
        return redirect()->route('admin.list');
    }

    public function unblock($id){
        $admin = Admin::find($id);
        $admin->status =1;
        $admin->save();

        session()->flash('success','Unblocked Done');
        return redirect()->route('admin.list');
    }

    public function delete($id){
        $admin = Admin::find($id);
        if (!is_null($admin)){
//          Delete Category Image
            if (File::exists('images/admin/'.$admin->image)){
                File::delete('images/admin/'.$admin->image);
            }
            $admin->delete();
        }

        session()->flash('success','Admin Has Deleted Successfully');

        return redirect()->route('admin.list');
    }
}
