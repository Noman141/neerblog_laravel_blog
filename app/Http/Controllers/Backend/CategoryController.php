<?php

namespace App\Http\Controllers\Backend;

use Intervention\Image\Facades\Image as Image;
use App\Models\Category;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller{
    public function __construct(){
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $categories = Category::orderBy('name','asc')->get();
        return view('backend.pages.category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $main_category = Category::orderBy('name','asc')->where('parent_id',NULL)->get();
        return view('backend.pages.category.create',compact('main_category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $request->validate([
            'name'    => 'required',
            'image'   => 'nullable | image'
        ]);

        $category = new Category();

        $category->name  = $request->name;
        $category->parent_id  = $request->parent_id;
//insert image
        if ($request->image > 0){
            $image = $request->file('image');
            $image_name = 'neershop'.'-'.$category->name.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('images/category/'.$image_name);
            Image::make($image)->save($location);
            $category->image = $image_name;
        }
        $category->save();
        session()->flash('success','A New category Has Created');
        return redirect()->route('admin.category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $main_category = Category::orderBy('name','asc')->where('parent_id',NULL)->get();
        $category = Category::find($id);
        if (!is_null($category)) {
            return view('backend.pages.category.edit',compact('category','main_category'));
        }else{
            return redirect()->route('admin.manage_category');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $request->validate([
            'name'   =>'required',
            'image'  =>'nullable|image'
        ]);

        $category = Category::find($id);
        $category->name = $request->name;
        $category->parent_id = $request->parent_id;

//        delete old image
        if (File::exists('images/category/'.$category->image)){
            File::delete('images/category/'.$category->image);
        }
        //insert image
        if ($request->image > 0){
            $image = $request->file('image');
            $image_name = 'neershop'.'-'.$category->name.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('images/category/'.$image_name);
            Image::make($image)->save($location);
            $category->image = $image_name;
        }
        $category->save();
        session()->flash('success','Category Has Updated');
        return redirect()->route('admin.category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id){
        $category = Category::find($id);

        if (!is_null($category)){
//          If it is a parent category then delete all its sub category
            if ($category->parent_id == NULL){
                $sub_category = Category::orderBy('name','asc')->where('parent_id',$category->id)->get();
                foreach ($sub_category as $sub){
                    if (File::exists('img/category/'.$sub->image)){
                        File::delete('img/category/'.$sub->image);
                    }
                    $sub->delete();
                }
            }
        }

//        delete image
        if (File::exists('images/category/'.$category->image)){
            File::delete('images/category/'.$category->image);
        }

        $category->delete();
        session()->flash('success','Category Has Deleted Successfully');

        return redirect()->route('admin.category');
    }
}
