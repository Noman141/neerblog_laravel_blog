<?php

namespace App\Http\Controllers\Backend;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller{

    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(){
        $comments = Comment::orderBy('id','desc')->get();
        return view('backend.pages.comment.index',compact('comments'));
    }

    public function show($id){
        $comment = Comment::find($id);
        return view('backend.pages.comment.show',compact('comment'));
    }


    public function publish($id){
        $comment = Comment::find($id);
        $comment->status = 1;
        $comment->save();

        session()->flash('success','Comments Published Done');
        return redirect()->route('admin.comments');
    }

    public function delete($id){
        $comment = Comment::find($id);
        $comment->delete();
        session()->flash('success','Subscriber Has Deleted Successfully');

        return redirect()->route('admin.comments');
    }

}
