<?php

namespace App\Http\Controllers\Backend;

use App\Models\Product;
use Intervention\Image\Facades\Image as Image;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use File;

use App\Http\Controllers\Controller;

class PostController extends Controller{
    public function __construct(){
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('id','desc')->get();
        return view('backend.pages.post.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $main_category = Category::orderBy('name','asc')->where('parent_id',NULL)->get();
        return view('backend.pages.post.create',compact('main_category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title'      => 'string|required|max:100',
            'body'       =>'string|required',
            'image'      =>'nullable|image',
            'tags'        =>'required',
            'category_id' => 'required|numeric',
            'admin_id'    => 'nullable|numeric',
        ]);

        $post = new Post();

        $post->title = $request->title;
        $post->body  = strip_tags($request->body);
        $post->category_id = $request->category_id;
        $post->admin_id = $request->admin_id;
        $post->tags = implode(',',$request->tags);
        $post->slug = str_slug($request->title);
//        $post->status = str_slug($request->status);

        if ($request->image >0){
            $image = $request->file('image');
            $image_name = 'neershop'.'-'.'posts'.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('images/post/'.$image_name);
            Image::make($image)->save($location);
            $post->image = $image_name;
        }
        $post->save();

        session()->flash('success','A New Post Has Created');
        return redirect()->route('admin.post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function publish($id){
        $post = Post::find($id);
        $post->status = 1;
        $post->save();

        session()->flash('success','Post Published Done');
        return redirect()->route('admin.post');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $post = Post::find($id);
        return view('backend.pages.post.edit',compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $request->validate([
            'title'      => 'string|required|max:100',
            'body'       =>'string|required',
            'image'      =>'nullable|image',
            'tags'        =>'required',
            'category_id' => 'required|numeric',
            'admin_id'    => 'nullable|numeric',
        ]);

        $post = Post::find($id);
        $post->title = $request->title;
        $post->body  = strip_tags($request->body);
        $post->category_id = $request->category_id;
        $post->admin_id = $request->admin_id;
        $post->tags = implode(',',$request->tags);
        $post->slug = str_slug($request->title);
//        $post->status = str_slug($request->status);

        if ($request->image >0){
            $image = $request->file('image');
            $image_name = 'neershop'.'-'.'posts'.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('images/post/'.$image_name);
            Image::make($image)->save($location);
            $post->image = $image_name;
        }
        $post->save();

        session()->flash('success','A New Post Has Updated');
        return redirect()->route('admin.post');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id){
        $post = Post::find($id);

        if (!is_null($post)){
//
//          Delete Category Image
            if (File::exists('images/post/'.$post->image)){
                File::delete('images/post/'.$post->image);
            }
            $post->delete();
        }

        session()->flash('success','Post Has Deleted Successfully');

        return redirect()->route('admin.post');

    }
}
