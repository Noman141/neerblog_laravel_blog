<?php

namespace App\Http\Controllers\Backend;

use App\Models\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Intervention\Image\Facades\Image as Image;

class SliderController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $sliders = Slider::orderBy('priority','asc')->get();
        return view('backend.pages.slider.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){

        return view('backend.pages.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $request->validate([
            'post_title'    => 'required|string|max:255',
            'post_body'     => 'nullable|string',
            'image'         => 'required|image',
            'priority'      => 'required|numeric',
            'btn_text'      => 'nullable|string|max:50',
            'btn_link'      => 'nullable|url',
        ]);

        $slider = new Slider();

        $slider->post_title = $request->post_title;
        $slider->post_body = $request->post_body;
        $slider->priority = $request->priority;
        $slider->btn_text = $request->btn_text;
        $slider->btn_link = $request->btn_link;

        if ($request->image > 0){
            $image = $request->file('image');
            $image_name = 'neershop-slider'.$request->priority.'.'.$image->getClientOriginalExtension();
            $location = public_path('images/slider/'.$image_name);
            Image::make($image)->save($location);
            $slider->image = $image_name;
        }
        $slider->save();
        session()->flash('success','A new slider has created');
        return redirect()->route('admin.slider');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){

        $slider = Slider::find($id);
        return view('backend.pages.slider.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $request->validate([
            'post_title'    => 'required|string|max:255',
            'post_body'     => 'nullable|string',
            'image'         => 'nullable|image',
            'priority'      => 'required|numeric',
            'btn_text'      => 'nullable|string|max:50',
            'btn_link'      => 'nullable|url',
        ]);

        $slider = Slider::find($id);

        $slider->post_title = $request->post_title;
        $slider->post_body = $request->post_body;
        $slider->priority = $request->priority;
        $slider->btn_text = $request->btn_text;
        $slider->btn_link = $request->btn_link;

        //        delete old image
        if (File::exists('images/slider/'.$slider->image)){
            File::delete('images/slider/'.$slider->image);
        }

        if ($request->image > 0){
            $image = $request->file('image');
            $image_name = 'neershop-slider'.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('images/slider/'.$image_name);
            Image::make($image)->save($location);
            $slider->image = $image_name;
        }
        $slider->save();
        session()->flash('success','Slider has Updated');
        return redirect()->route('admin.slider');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id){

        $slider = Slider::find($id);

//        delete image
        if (File::exists('images/slider/'.$slider->image)){
            File::delete('images/slider/'.$slider->image);
        }

        $slider->delete();
        session()->flash('success','Slider Has Deleted Successfully');

        return redirect()->route('admin.slider');
    }
}
