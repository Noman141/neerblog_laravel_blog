<?php

namespace App\Http\Controllers\Backend;

use App\Models\SocalMedia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SocalMediaController extends Controller{

    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(){

        $links = SocalMedia::first();
//        dd($links);
        return view('backend.pages.socal-media-link.index',compact('links'));
    }

    public function edit($id){
        $links = SocalMedia::find($id);
        return view('backend.pages.socal-media-link.edit',compact('links'));
    }

    public function update(Request $request, $id){
        $request->validate([
           'facebook_link'    => 'nullable|url',
           'twitter_link'    => 'nullable|url',
           'linkedin_link'    => 'nullable|url',
           'instagram_link'    => 'nullable|url',
           'dribbble_link'    => 'nullable|url',
        ]);

        $links = SocalMedia::find($id);

        $links->facebook_link = $request->facebook_link;
        $links->twitter_link = $request->twitter_link;
        $links->linkedin_link = $request->linkedin_link;
        $links->instagram_link = $request->instagram_link;
        $links->dribbble_link = $request->dribbble_link;

        $links->save();

        session()->flash('success','Socal Media link has updated Successfully');
        return redirect()->route('admin.socalmeidalinks');

    }
}
