<?php

namespace App\Http\Controllers\Backend;

use App\Models\Subscriber;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriberController extends Controller{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    public function index(){
        $subscribers = Subscriber::orderBy('id','desc')->get();
        return view('backend.pages.subscriber.index',compact('subscribers'));
    }

    public function approve(Request $request,$id){
        $request->validate([
           'status'  => 'nullable|numeric'
        ]);

        $subscriber = Subscriber::find($id);
        $subscriber->status = 1;
        $subscriber->save();

        session()->flash('success','Subscriber Approved Done');
        return redirect()->route('admin.subscribers');
    }

    public function delete($id){
        $subscriber = Subscriber::find($id);
        $subscriber->delete();
        session()->flash('success','Subscriber Has Deleted Successfully');

        return redirect()->route('admin.subscribers');
    }

}
