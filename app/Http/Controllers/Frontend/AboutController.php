<?php

namespace App\Http\Controllers\frontend;

use App\Models\Admin;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller{
    public function index(){
        $admins = Admin::orderBy('id','asc')->where('status',1)->get();
        return view('frontend.pages.about.index',compact('admins'));
    }

    public function show($slug){
        $admin = Admin::where('slug',$slug)->first();
        $posts = Post::where('admin_id',$admin->id)->get();
        return view('frontend.pages.about.show',compact('admin','posts'));
    }
}
