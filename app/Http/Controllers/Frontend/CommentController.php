<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller{
    public function store(Request $request){
        $request->validate([
           'name'        => 'required|string',
           'email'       => 'required|email',
           'comment'     => 'required',
           'post_id'     => 'numeric',
        ]);

        $comment = new Comment();

        $comment->name = $request->name;
        $comment->email = $request->email;
        $comment->comment = $request->comment;
        $comment->post_id = $request->post_id;
        $comment->save();
        return redirect()->route('post.show',$comment->post->slug);
    }

}
