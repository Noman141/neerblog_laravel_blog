<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller{
    public function index(){
        return view('frontend.pages.contact.index');
    }

    public function store(Request $request){
        $request->validate([
            'name'     => 'required|string',
            'email'    => 'required|email',
            'subject'  => 'nullable|string',
            'message'  => 'required|string',
        ]);

        $contact = new Contact();

        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->subject = $request->subject;
        $contact->message = $request->message;
        $contact->save();

        session()->flash('success','Message Sent. You will get your reply via email ASAP');
        return redirect()->route('contact');
    }
}
