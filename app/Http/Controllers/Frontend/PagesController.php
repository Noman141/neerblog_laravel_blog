<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Post;
use App\Models\Slider;
use App\Models\SocalMedia;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;

class PagesController extends Controller{
    public function index(){
        $categories = Category::all();
        $main_categories = Category::orderBy('name','asc')->where('parent_id',NULL)->get();
        $sliders = Slider::orderBy('priority','asc')->get();
        $posts = Post::orderBy('id','desc')->where('status',1)->paginate(5);
        return view('frontend.pages.posts.index',compact('main_categories','categories','sliders','posts'));
    }

    public function search(Request $request){
        $categories = Category::all();
        $main_categories = Category::orderBy('name','asc')->where('parent_id',NULL)->get();

        $search = $request->search;
        $posts = Post::orwhere('title','like','%'.$search.'%')
                       ->orwhere('body','like','%'.$search.'%')
                       ->orwhere('tags','like','%'.$search.'%')
                       ->orderBy('id','desc')
                       ->paginate(3);
        return view('frontend.pages.posts.postsbysearch',compact('categories','main_categories','posts','search'));
    }

    public function showingPostsByParentCategory($id){
        $parent_category = Category::orderBy('name','asc')->where('id',$id)->find($id);
        $child_category = Category::orderBy('name','asc')->where('parent_id',$parent_category->id)->first();

        $posts = Post::orderBy('id','desc')->where('category_id',$child_category->id)->paginate(3);
        return view('frontend.pages.posts.postsbyparentcategory',compact('parent_category','child_category','posts'));
    }

}
