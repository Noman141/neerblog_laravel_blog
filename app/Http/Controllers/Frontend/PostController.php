<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller{

    public function show($slug){
        $categories = Category::all();
        $main_categories = Category::orderBy('name','asc')->where('parent_id',NULL)->get();
        $post = Post::where('slug', $slug)->first();
        return view('frontend.pages.posts.show',compact('categories','main_categories','post'));
    }

    public  function postByCategory($id){
        $category = Category::find($id);
        if (!is_null($category)){
            return view('frontend.pages.posts.postsbycategory',compact('category'));
        }

    }
}
