<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Subscriber;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Alert;

class SubscriberController extends Controller{

    public function create(){
        return view('frontend.pages.subscribe.index');
    }

    public function store(Request $request){
        $request->validate([
            'name' =>'required|string',
            'email' =>'required|email',
        ]);

        $subscriber = new  Subscriber();

        $subscriber->name = $request->name;
        $subscriber->email = $request->email;
        $subscriber->save();

        Alert::message('Subscribe Done!');
        return redirect()->route('index');
    }
}
