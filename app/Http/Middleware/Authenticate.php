<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request,$guard = null)
    {
        switch ($guard){
            case 'admin':
                if (Auth::guard($guard)->check()){
                    if (! $request->expectsJson()) {
                        return route('admin.login');
                    }
                }
                break;
        }

    }
}
