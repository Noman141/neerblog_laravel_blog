<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model{
    protected $fillable = [
        'admin_id','category_id','title','body','image','tags','slug','status','reply','replied_by'
    ];

    public function category(){
        return $this->belongsTo(Category::class);
   }


    public function admin(){
        return $this->belongsTo(Admin::class);
    }

}
