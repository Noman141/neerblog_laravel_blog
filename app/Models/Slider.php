<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model{

    protected $fillable = [
      'image','post_title','post_body','btn_text','btn_link','priority'
    ];
}
