<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SocalMedia extends Model{

    protected $fillable = [
      'facebook_link','twitter_link','linkedin_link','dribbble_link','instagram_link'
    ];
}
