@extends('backend.layouts.master')
@section('title')
    | Category
@endsection
@section('content')
    <div class="container-scroller">
        @include('backend.partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('backend.partials.sidenav')
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h2>Create New Category</h2>
                            </div>
                            <div class="col-md-6 d-flex justify-content-end">
                                <a href="{{ route('admin.category') }}" class="btn btn-info ">Category List</a>
                            </div>
                        </div>
                        <hr>
                    @include('global.msg')
                    <!-- Horizontal material form -->
                        <form method="post" action="{{ route('admin.category.update',$category->id) }}" enctype="multipart/form-data">
                        @csrf
                        <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                <label for="name" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <div class="md-form mt-0">
                                        <input type="text" class="form-control" id="name" name="name" value="{{ $category->name }}">
                                    </div>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                <label for="parent_id" class="col-sm-2 col-form-label">Parent Category</label>
                                <div class="col-sm-10">
                                    <select class="browser-default custom-select" name="parent_id" id="parent_id">
                                        <option selected disabled>Select One---</option>
                                        @foreach($main_category as $maincat)
                                            <option value="{{ $maincat->id }}"{{ $maincat->id == $category->parent_id ?'selected':NULL}}>{{ $maincat->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                <label class="col-sm-3 col-form-label">Your Old Image</label>
                                <div class="col-sm-9">
                                    <div class="md-form mt-0">
                                        <img src="{{ asset('images/category/'.$category->image) }}" alt="" width="150px;" height="120px;">
                                    </div>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                <label for="image" class="col-sm-2 col-form-label">Image</label>
                                <div class="col-sm-10">
                                    <div class="md-form mt-0">
                                        <input type="file" class="form-control" id="image" placeholder="Name" name="image">
                                    </div>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <label  class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary btn-md">Add Category</button>
                                </div>
                            </div>
                            <!-- Grid row -->
                        </form>
                        <!-- Horizontal material form -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection