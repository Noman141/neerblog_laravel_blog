@extends('backend.layouts.master')

@section('content')
    <div class="container-scroller">
        @include('backend.partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('backend.partials.sidenav')
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                            <div class="card card-statistics">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="fas fa-cube text-danger icon-lg"></i>
                                        </div>
                                        <div class="float-right">
                                            <p class="card-text text-right">Total Revenue</p>
                                            <div class="fluid-container">
                                                <h3 class="card-title font-weight-bold text-right mb-0">$65,650</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="text-muted mt-3">
                                        <i class="fas fa-exclamation-triangle mr-1"></i>
                                        65% lower growth
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                            <div class="card card-statistics">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="fas fa-receipt text-warning icon-lg"></i>
                                        </div>
                                        <div class="float-right">
                                            <p class="card-text text-right">Orders</p>
                                            <div class="fluid-container">
                                                <h3 class="card-title font-weight-bold text-right mb-0">3455</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="text-muted mt-3">
                                        <i class="far fa-bookmark mr-1"></i> Product-wise sales
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                            <div class="card card-statistics">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="fas fa-poll text-teal icon-lg"></i>
                                        </div>
                                        <div class="float-right">
                                            <p class="card-text text-right">Sales</p>
                                            <div class="fluid-container">
                                                <h3 class="card-title font-weight-bold text-right mb-0">5693</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="text-muted mt-3">
                                        <i class="far fa-calendar-alt mr-1"></i> Weekly Sales
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
                            <div class="card card-statistics">
                                <div class="card-body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="fas fa-user-alt text-info icon-lg"></i>
                                        </div>
                                        <div class="float-right">
                                            <p class="card-text text-right">Employees</p>
                                            <div class="fluid-container">
                                                <h3 class="card-title font-weight-bold text-right mb-0">246</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="text-muted mt-3">
                                        <i class="fas fa-sync-alt mr-1"></i> Product-wise sales
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 grid-margin">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title mb-4">Targets</h5>
                                    <canvas id="dashoard-area-chart" height="100px"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <!-- content-wrapper ends -->
                @include('backend.partials.footer')
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->

@endsection