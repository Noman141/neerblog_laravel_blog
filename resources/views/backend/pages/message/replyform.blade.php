@extends('backend.layouts.master')
@section('title')
    | Message | Reply
@endsection
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
@endsection
@section('content')
    <div class="container-scroller">
        @include('backend.partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('backend.partials.sidenav')
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h2>Message Reply To {{ $message->name }}</h2>
                            </div>
                            <div class="col-md-6 d-flex justify-content-end">
                                <a class="btn btn-info btn-sm " href="{{ route('admin.messages') }}">Message List</a>
                            </div>
                        </div>
                        <hr>
                        <!-- Grid row -->
                        <div class="row">

                            <!-- Grid column -->
                            <div class="col-md-8 mb-md-0 mb-5">
                                @include('global.msg')
                                <form action="{{ route('admin.message.reply',$message->id) }}" method="post">
                                @csrf
                                <!-- Grid row -->
                                    <div class="row">

                                        <!-- Grid column -->
                                        <div class="col-md-12">
                                            <div class="md-form mb-0">
                                                <input type="text" id="contact-name" class="form-control" name="replied_by" value="{{ Auth::user()->username }}" readonly>
                                                <label for="contact-name" class="">Send From Email</label>
                                            </div>
                                        </div>
                                        <!-- Grid column -->

                                        <!-- Grid column -->
                                        <div class="col-md-6">
                                            <div class="md-form mb-0">
                                                <input type="text" id="contact-name" class="form-control" name="name" value="{{ $message->name }}" readonly>
                                                <label for="contact-name" class="">Send To Name</label>
                                            </div>
                                        </div>
                                        <!-- Grid column -->

                                        <!-- Grid column -->
                                        <div class="col-md-6">
                                            <div class="md-form mb-0">
                                                <input type="text" id="contact-email" class="form-control" name="email" value="{{ $message->email }}" readonly>
                                                <label for="contact-email" class="">Send To Email</label>
                                            </div>
                                        </div>
                                        <!-- Grid column -->

                                    </div>
                                    <!-- Grid row -->

                                    <!-- Grid row -->
                                    <div class="row">

                                        <!-- Grid column -->
                                        <div class="col-md-12">
                                            <div class="md-form mb-0">
                                                <input type="text" id="contact-Subject" class="form-control" name="subject" value="{{ $message->subject }}" readonly>
                                                <label for="contact-Subject" class="">Subject</label>
                                            </div>
                                        </div>
                                        <!-- Grid column -->

                                    </div>
                                    <!-- Grid row -->

                                    <!-- Grid row -->
                                    <div class="row">

                                        <!-- Grid column -->
                                        <div class="col-md-12">
                                            <div class="md-form">
                                                <textarea type="text" id="contact-message" name="reply" class="form-control md-textarea" rows="3"></textarea>
                                                <label for="contact-message">Your Reply</label>
                                            </div>
                                        </div>
                                        <!-- Grid column -->
                                        <div class="text-center text-md-left">
                                            <button type="submit" class="btn btn-info btn-md">Send</button>
                                        </div>

                                    </div>
                                    <!-- Grid row -->

                                </form>

                            </div>
                            <!-- Grid column -->

                            <!-- Grid column -->
                            <div class="col-md-4 text-center">
                                <ul class="list-unstyled mb-0">
                                    <li>
                                        <i class="fas fa-map-marker-alt fa-2x text-default"></i>
                                        <p>San Francisco, CA 94126, USA</p>
                                    </li>
                                    <li>
                                        <i class="fas fa-phone fa-2x mt-4 text-default"></i>
                                        <p>+ 01 234 567 89</p>
                                    </li>
                                    <li>
                                        <i class="fas fa-envelope fa-2x mt-4 text-default"></i>
                                        <p class="mb-0">contact@example.com</p>
                                    </li>
                                </ul>
                            </div>
                            <!-- Grid column -->

                        </div>
                        <!-- Grid row -->

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

