@extends('backend.layouts.master')
@section('title')
    | Post | Create
@endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/backend/addmultipletags.css') }}">
    <style>
        .ck.ck-editor__main > .ck-editor__editable:not(.ck-focused){
            height: 200px !important;
        }
    </style>
@endsection
@section('content')
    <div class="container-scroller">
        @include('backend.partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('backend.partials.sidenav')
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h2>Create New Posts</h2>
                            </div>
                            <div class="col-md-6 d-flex justify-content-end">
                                <a href="{{ route('admin.post') }}" class="btn btn-info ">Post List</a>
                            </div>
                        </div>
                        <hr>
                    @include('global.msg')
                    <!-- Horizontal material form -->
                        <form method="post" action="{{ route('admin.post.store') }}" enctype="multipart/form-data" >
                        @csrf
                        <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                <label for="title" class="col-sm-2 col-form-label">Title</label>
                                <div class="col-sm-10">
                                    <div class="md-form mt-0">
                                        <input type="text" class="form-control" id="title" placeholder="Tilte" name="title">
                                    </div>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->

                            <input type="hidden" class="form-control" id="admin_id"  name="admin_id" value="{{ Auth::user()->id }}">

                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                <label for="body" class="col-sm-2 col-form-label">Body</label>
                                <div class="col-sm-10">
                                    <div class="md-form mt-0">
                                        <textarea id="editor" class="form-control" name="body" style="background-color: #fff;height: 200px" rows="30"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                <label for="category_id" class="col-sm-2 col-form-label">Post Category</label>
                                <div class="col-sm-10">
                                    <select class="browser-default custom-select" name="category_id" id="category_id">
                                        <option selected disabled>Select One---</option>
                                        @foreach(App\Models\Category::orderBy('name','asc')->where('parent_id',NULL)->get() as $parent)
                                            <option value="{{ $parent->id }}">{{ $parent->name }}</option>
                                            @foreach(App\Models\Category::orderBy('name','asc')->where('parent_id',$parent->id)->get() as $child)
                                                <option value="{{ $child->id }}">------>{{ $child->name }}</option>
                                            @endforeach
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                <label for="form-tags-1" class="col-sm-2 col-form-label">Tags</label>
                                <div class="col-sm-10">
                                    <div class="md-form mt-0">
                                        <input id="form-tags-1" name="tags[]" type="text" >
                                    </div>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                <label for="image" class="col-sm-2 col-form-label">Image</label>
                                <div class="col-sm-10">
                                    <div class="md-form mt-0">
                                        <input type="file" class="form-control" id="image" placeholder="Name" name="image">
                                    </div>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <label  class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <button type="submit"  class="btn btn-primary btn-md">Add New Post</button>
                                </div>
                            </div>
                            <!-- Grid row -->
                        </form>
                        <!-- Horizontal material form -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/backend/addmuslipletags.js') }}"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ) )
            .catch( error => {
            console.error( error );
        } );
    </script>
@endsection
