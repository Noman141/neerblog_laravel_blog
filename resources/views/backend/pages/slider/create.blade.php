@extends('backend.layouts.master')
@section('title')
    | Slider | Create
@endsection
@section('content')
    <div class="container-scroller">
        @include('backend.partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('backend.partials.sidenav')
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h2>Create New Slider</h2>
                            </div>
                            <div class="col-md-6 d-flex justify-content-end">
                                <a href="{{ route('admin.slider') }}" class="btn btn-info ">Slider List</a>
                            </div>
                        </div>
                        <hr>
                    @include('global.msg')
                    <!-- Horizontal material form -->
                        <form method="post" action="{{ route('admin.slider.store') }}" enctype="multipart/form-data" >
                        @csrf
                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                <label for="post_title" class="col-sm-2 col-form-label">Post Title</label>
                                <div class="col-sm-10">
                                    <div class="md-form mt-0">
                                        <input type="text" class="form-control" id="post_title" placeholder="Post Title" name="post_title">
                                    </div>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                <label for="post_body" class="col-sm-2 col-form-label">Post Body</label>
                                <div class="col-sm-10">
                                    <div class="md-form mt-0">
                                        <textarea id="post_body" class="form-control" name="post_body" style="background-color: #fff;height: 100px" rows="30"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                <label for="btn_text" class="col-sm-2 col-form-label">Button Text</label>
                                <div class="col-sm-10">
                                    <div class="md-form mt-0">
                                        <input type="text" class="form-control" id="btn_text" placeholder="Button Text" name="btn_text">
                                    </div>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                <label for="btn_link" class="col-sm-2 col-form-label">Button Link</label>
                                <div class="col-sm-10">
                                    <div class="md-form mt-0">
                                        <input type="url" class="form-control" id="btn_link" placeholder="Button Link" name="btn_link">
                                    </div>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                <label for="priority" class="col-sm-2 col-form-label">Priority</label>
                                <div class="col-sm-10">
                                    <div class="md-form mt-0">
                                        <input type="number" class="form-control" id="priority" placeholder="Priority" name="priority">
                                    </div>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                <label for="image" class="col-sm-2 col-form-label">Image</label>
                                <div class="col-sm-10">
                                    <div class="md-form mt-0">
                                        <input type="file" class="form-control" id="image" placeholder="Name" name="image">
                                    </div>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <label  class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <button type="submit" name="submit" class="btn btn-primary btn-md">Add Slider</button>
                                </div>
                            </div>
                            <!-- Grid row -->
                        </form>
                        <!-- Horizontal material form -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.partials.footer')
@endsection
