@extends('backend.layouts.master')
@section('title')
    | Category | Socal Media links
@endsection
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
@endsection
@section('content')
    <div class="container-scroller">
        @include('backend.partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('backend.partials.sidenav')
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h2>Socal Media Link's Edit</h2>
                            </div>

                            <div class="col-md-6 d-flex justify-content-end">
                                <a class="btn btn-sm btn-info" href="{{ route('admin.socalmeidalinks') }}">Link's List</a>
                            </div>
                        </div>
                        <hr>
                        <!-- Horizontal material form -->
                        <form action="{{ route('admin.socalmeidalink.update',$links->id) }}" method="post">
                            @csrf
                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                <label for="facebook_link" class="col-sm-2 col-form-label">Facebook Link</label>
                                <div class="col-sm-10">
                                    <div class="md-form mt-0">
                                        <input type="url" name="facebook_link" class="form-control" id="facebook_link" value="{{ $links->facebook_link }}">
                                    </div>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                <label for="twitter_link" class="col-sm-2 col-form-label">Twitter Link</label>
                                <div class="col-sm-10">
                                    <div class="md-form mt-0">
                                        <input type="url" name="twitter_link" class="form-control" id="twitter_link" value="{{ $links->twitter_link }}">
                                    </div>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                <label for="linkedin_link" class="col-sm-2 col-form-label">Linkedin Link</label>
                                <div class="col-sm-10">
                                    <div class="md-form mt-0">
                                        <input type="url" name="linkedin_link" class="form-control" id="linkedin_link" value="{{ $links->linkedin_link }}">
                                    </div>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                <label for="instagram_link" class="col-sm-2 col-form-label">Instagram Link</label>
                                <div class="col-sm-10">
                                    <div class="md-form mt-0">
                                        <input type="url" name="instagram_link" class="form-control" id="instagram_link" value="{{ $links->instagram_link }}">
                                    </div>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <!-- Material input -->
                                {{--<i class="fas fa-lock prefix"></i>--}}
                                <label for="dribbble_link" class="col-sm-2 col-form-label">Dribbble Link</label>
                                <div class="col-sm-8">
                                    <div class="md-form mt-0">
                                        <input type="url" name="dribbble_link" class="form-control" id="dribbble_link" value="{{ $links->dribbble_link }}">
                                    </div>
                                </div>
                            </div>
                            <!-- Grid row -->

                            <!-- Grid row -->
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-primary btn-md">Update Links</button>
                                </div>
                            </div>
                            <!-- Grid row -->
                        </form>
                        <!-- Horizontal material form -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

