@extends('backend.layouts.master')
@section('title')
    | Category | Socal Media links
@endsection
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
@endsection
@section('content')
    <div class="container-scroller">
        @include('backend.partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('backend.partials.sidenav')
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h2>Socal Media Link's Details</h2>
                            </div>
                            <div class="col-md-6 d-flex justify-content-end">
                                <a class="btn btn-info btn-sm" href="{{ route('admin.socalmeidalink.edit',$links->id) }}">Edit Link's</a>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-2">
                                <h6>Facebook Link :</h6>
                            </div>
                            <div class="col-md-10 ">
                                <strong>{{ $links->facebook_link }}</strong>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-2">
                                <h6>Twitter Link :</h6>
                            </div>
                            <div class="col-md-10 ">
                                <strong>{{ $links->twitter_link }}</strong>
                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-md-2">
                                <h6>Linkedin Link :</h6>
                            </div>
                            <div class="col-md-10 ">
                                <strong>{{ $links->linkedin_link }}</strong>
                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-md-2">
                                <h6>Dribbble Link :</h6>
                            </div>
                            <div class="col-md-10 ">
                                <strong>{{ $links->dribbble_link }}</strong>
                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-md-2">
                                <h6>Instagram Link :</h6>
                            </div>
                            <div class="col-md-10 ">
                                <strong>{{ $links->instagram_link }}</strong>
                            </div>
                        </div>
                        <hr>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

