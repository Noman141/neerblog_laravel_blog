<!-- plugins:css -->
<link rel="stylesheet" href="{{ asset('css/backend/materialdesignicons.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/backend/simple-line-icons.css') }}">
<!-- endinject -->
<!-- plugin css for this page -->
<link rel="stylesheet" href="{{ asset('css/backend/bootstrap.min.css') }}">

<link rel="stylesheet" href="{{ asset('css/backend/mdb.min.css') }}">
<!-- End plugin css for this page -->
<!-- inject:css -->
<link rel="stylesheet" href="{{ asset('css/backend/style.css') }}?id=1">
{{--font awesome--}}
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

<!-- endinject -->
<link rel="shortcut icon" href="{{ asset('img/backend/favicon.png') }}" />

@yield('style')