<!-- partial:partials/_footer.html -->
<footer class="footer bg-dark ">
    <div class="container-fluid clearfix">
        <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018 <a href="http://www.bootstrapdash.com/" target="_blank">NeerBlog</a>. All rights reserved.</span>
        <span class="text-muted float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="fas fa-heart text-danger"></i></span>
    </div>
</footer>
<!-- partial -->