<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <div class="nav-link">
                <div class="profile-image"> <img src="{{ asset('images/admin/'.Auth::user()->image) }}" alt="{{ Auth::user()->name }}"/> <span class="online-status online"></span> </div>
                <div class="profile-name">
                    <p class="name">{{ Auth::user()->name }}</p>
                    <p class="designation">{{ Auth::user()->role }}</p>
                    <div class="badge badge-teal mx-auto mt-3">Online</div>
                </div>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.dashboard') }}"><img class="menu-icon" src="{{ asset('img/backend/01.png') }}" alt="menu icon"><span class="menu-title">Dashboard</span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href=""><img class="menu-icon" src="{{ asset('img/backend/02.png') }}" alt="menu icon"><span class="menu-title">Widgets</span></a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#admin-pages" aria-expanded="false" aria-controls="general-pages"> <img class="menu-icon" src="{{ asset('img/backend/04.png') }}" alt="menu icon"> <span class="menu-title">Manage Admin</span><i class="fas fa-angle-double-down ml-auto"></i></a>
            <div class="collapse" id="admin-pages">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.list') }}">Admin List</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.register.form') }}">Create Admin</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#Manage_slider" aria-expanded="false" aria-controls="general-pages"> <img class="menu-icon" src="{{ asset('img/backend/04.png') }}" alt="menu icon"> <span class="menu-title">Manage Slider</span><i class="fas fa-angle-double-down ml-auto"></i></a>
            <div class="collapse" id="Manage_slider">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.slider') }}">Slider List</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.slider.create') }}">Create Slider</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#general-pages" aria-expanded="false" aria-controls="general-pages"> <img class="menu-icon" src="{{ asset('img/backend/04.png') }}" alt="menu icon"> <span class="menu-title">Manage Post</span><i class="fas fa-angle-double-down ml-auto"></i></a>
            <div class="collapse" id="general-pages">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.post') }}">Post List</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.post.create') }}">Add Post</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#manageCategory" aria-expanded="false" aria-controls="general-pages"> <img class="menu-icon" src="{{ asset('img/backend/04.png') }}" alt="menu icon"> <span class="menu-title">Manage Category</span><i class="fas fa-angle-double-down ml-auto"></i></a>
            <div class="collapse" id="manageCategory">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.category') }}">Category List</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.category.create') }}">Add Category</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#manageSubscribers" aria-expanded="false" aria-controls="general-pages"> <img class="menu-icon" src="{{ asset('img/backend/04.png') }}" alt="menu icon"> <span class="menu-title">Manage Subscriber</span><i class="fas fa-angle-double-down ml-auto"></i></a>
            <div class="collapse" id="manageSubscribers">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.subscribers') }}">Subscribers List</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#manageComments" aria-expanded="false" aria-controls="general-pages"> <img class="menu-icon" src="{{ asset('img/backend/04.png') }}" alt="menu icon"> <span class="menu-title">Manage Comments</span><i class="fas fa-angle-double-down ml-auto"></i></a>
            <div class="collapse" id="manageComments">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.comments') }}">Comment List</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#manageSocalMediaLink" aria-expanded="false" aria-controls="general-pages"> <img class="menu-icon" src="{{ asset('img/backend/04.png') }}" alt="menu icon"> <span class="menu-title">Manage Link</span><i class="fas fa-angle-double-down ml-auto"></i></a>
            <div class="collapse" id="manageSocalMediaLink">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.socalmeidalinks') }}">Socal List</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a href="#" class="nav-link">
                <form action="{{ asset(route('admin.logout')) }}" class="form-inline" method="post">
                    @csrf
                    <input type="submit" value="Log Out" class="btn btn-danger btn-sm">
                </form>
            </a>
        </li>

    </ul>
</nav>
<!-- partial -->