@extends('frontend.layouts.master')

@section('title')
    NeerShop - About
@endsection

@section('content')
    <header>
        {{--uppernav--}}
        @include('frontend.partials.uppernav')

        {{--main nav--}}
        @include('frontend.partials.nav')
    </header>
    <section id="aboutus">

        <div class="container">
            <h2 class="mb-3">Our Team Members</h2>
            <hr>
            <div class="row">
                @foreach($admins as $admin)
                <div class="col-md-6">
                    <!-- Card -->
                    <div class="card testimonial-card">

                        <!-- Background color -->
                        <div class="card-up indigo lighten-1"></div>

                        <!-- Avatar -->
                        <div class="avatar mx-auto white">
                            <img src="{{ asset('images/admin/'.$admin->image) }}" class="rounded-circle" alt="woman avatar">
                        </div>

                        <!-- Content -->
                        <div class="card-body">
                            <!-- Name -->
                            <h4 class="card-title">Name :<strong>{{ $admin->name }}</strong></h4>
                            <hr>
                            <!-- Quotation -->
                            <h6 class="card-title">Designation : <strong>{{ $admin->role }}</strong></h6>

                            <a href="{{ route('about.show',$admin->slug) }}" class="btn btn-sm btn-info">View Profile</a>
                        </div>

                    </div>
                    <!-- Card -->
                </div>
                    @endforeach
            </div>
        </div>
    </section>

    @include('frontend.partials.footer')
@endsection