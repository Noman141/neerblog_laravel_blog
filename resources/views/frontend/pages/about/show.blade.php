@extends('frontend.layouts.master')

@section('title')
    NeerShop - About
@endsection

@section('content')
    <header>
        {{--uppernav--}}
        @include('frontend.partials.uppernav')

        {{--main nav--}}
        @include('frontend.partials.nav')
    </header>
    <section id="adminprofile">

        <div class="container">
            <!-- Section heading -->
            <h2 class="h1-responsive font-weight-bold text-center my-2">{{ $admin->name }}'s Profile</h2>
            <hr>
            <!-- Section description -->
            <p class="grey-text text-center w-responsive mx-auto mb-5">Duis aute irure dolor in reprehenderit in
                voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit est laborum.</p>

            <!-- Grid row -->
            <div class="row">

                <!-- Grid column -->
                <div class="col-lg-5 mb-lg-0 mb-5">
                    <!--Image-->
                    <img src="{{ asset('images/admin/'.$admin->image) }}" class="" alt="{{ $admin->name }}" height="600px" width="450px">
                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-lg-7">

                    <!-- Grid row -->
                    <div class="row mb-3">
                        <div class="col-md-1 col-2">
                            <i class="fas fa-user-alt fa-2x warning-text"></i>
                            {{--<i class="fas fa-user-alt warning-color"></i>--}}
                        </div>
                        <div class="col-md-11 col-10">
                            <h5 class="font-weight-bold mb-3">Personal Info</h5>
                            <p class="grey-text">Email : {{ $admin->email }}</p>
                            <p class="grey-text">Phone : {{ $admin->phone }}</p>
                        </div>
                    </div>
                    <!-- Grid row -->

                    <!-- Grid row -->
                    <div class="row mb-3">
                        <div class="col-md-1 col-2">
                            <i class="fas fa-book fa-2x cyan-text"></i>
                        </div>
                        <div class="col-md-11 col-10">
                            <h5 class="font-weight-bold mb-3">Education</h5>
                            <p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing. Reprehenderit maiores nam,
                                aperiam minima elit assumenda voluptate velit.</p>
                        </div>
                    </div>
                    <!-- Grid row -->

                    <!-- Grid row -->
                    <div class="row mb-3">
                        <div class="col-md-1 col-2">
                            <i class="fas fa-code fa-2x red-text"></i>
                        </div>
                        <div class="col-md-11 col-10">
                            <h5 class="font-weight-bold mb-3">Technology</h5>
                            <p class="grey-text">Lorem ipsum dolor sit amet, consectetur adipisicing. Reprehenderit maiores nam,
                                aperiam minima elit assumenda voluptate velit.</p>
                        </div>
                    </div>
                    <!-- Grid row -->

                    <!-- Grid row -->
                    <div class="row">
                        <div class="col-md-1 col-2">
                            <i class="far fa-money-bill-alt fa-2x deep-purple-text"></i>
                        </div>
                        <div class="col-md-11 col-10">
                            <h5 class="font-weight-bold mb-3">Finance</h5>
                            <p class="grey-text mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing. Reprehenderit maiores
                                nam, aperiam minima elit assumenda voluptate velit.</p>
                        </div>
                    </div>
                    <!-- Grid row -->

                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->
        </div>

    </section>
    <!-- Section: Blog v.3 -->
    <div class="container">
        <section class="my-5">
            <hr>

            <!-- Section heading -->
            <h2 class="h1-responsive font-weight-bold text-center my-5">Recent Posts By {{ $admin->name }}</h2>
            <hr>
            <!-- Section description -->
            <p class="text-center dark-grey-text w-responsive mx-auto mb-5">Duis aute irure dolor in reprehenderit in
                voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            @foreach($posts as $post)
            <!-- Grid row -->
            <div class="row">

                <!-- Grid column -->
                <div class="col-lg-5 col-xl-4">

                    <!-- Featured image -->
                    <div class="view overlay rounded z-depth-1-half mb-lg-0 mb-4">
                        <img class="img-fluid" src="{{ asset('images/post/'.$post->image) }}" alt="Sample image">
                        <a>
                            <div class="mask rgba-white-slight"></div>
                        </a>
                    </div>

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-lg-7 col-xl-8">

                    <!-- Post title -->
                    <a href="{{ route('post.show',$post->slug) }}">
                       <h3 class="font-weight-bold mb-3"><strong>{{ str_limit($post->title,30) }}</strong></h3>
                    </a>
                    <!-- Excerpt -->
                    <p class="dark-grey-text">{{ str_limit($post->body,150) }}</p>
                    <!-- Post data -->
                       <p>In <a href="{{ route('post.categoryposts.show',$post->category->id) }}" class="font-weight-bold">{{ $post->category->name }}</a>, {{ $post->created_at->format('jS F Y') }}</p>
                    <!-- Read more button -->
                    <a href="{{ route('post.show',$post->slug) }}" class="btn btn-primary btn-md">Read more</a>

                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->

            <hr class="my-5">
           @endforeach
        </section>
    </div>


    <!-- Section: Blog v.3 -->

    @include('frontend.partials.footer')
@endsection