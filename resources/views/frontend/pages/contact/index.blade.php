@extends('frontend.layouts.master')

@section('title')
    NeerShop - Contact
@endsection

@section('content')
    <header>
        {{--uppernav--}}
        @include('frontend.partials.uppernav')

        {{--main nav--}}
        @include('frontend.partials.nav')
    </header>
    <section id="contact">

        <div class="container">
            <!-- Section heading -->
            <h2 class="h1-responsive font-weight-bold text-center my-5">Contact us</h2>
            <hr>
            <!-- Section description -->
            <p class="text-center w-responsive mx-auto mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Fugit, error amet numquam iure provident voluptate esse quasi, veritatis totam voluptas nostrum quisquam
                eum porro a pariatur veniam.</p>

            <!-- Grid row -->
            <div class="row">

                <!-- Grid column -->
                <div class="col-md-8 mb-md-0 mb-5">
                    @include('global.msg')
                    <form action="{{ route('contact.store') }}" method="post">
                        @csrf
                        <!-- Grid row -->
                        <div class="row">

                            <!-- Grid column -->
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <input type="text" id="contact-name" class="form-control" name="name">
                                    <label for="contact-name" class="">Your name</label>
                                </div>
                            </div>
                            <!-- Grid column -->

                            <!-- Grid column -->
                            <div class="col-md-6">
                                <div class="md-form mb-0">
                                    <input type="text" id="contact-email" class="form-control" name="email">
                                    <label for="contact-email" class="">Your email</label>
                                </div>
                            </div>
                            <!-- Grid column -->

                        </div>
                        <!-- Grid row -->

                        <!-- Grid row -->
                        <div class="row">

                            <!-- Grid column -->
                            <div class="col-md-12">
                                <div class="md-form mb-0">
                                    <input type="text" id="contact-Subject" class="form-control" name="subject">
                                    <label for="contact-Subject" class="">Subject</label>
                                </div>
                            </div>
                            <!-- Grid column -->

                        </div>
                        <!-- Grid row -->

                        <!-- Grid row -->
                        <div class="row">

                            <!-- Grid column -->
                            <div class="col-md-12">
                                <div class="md-form">
                                    <textarea type="text" id="contact-message" name="message" class="form-control md-textarea" rows="3"></textarea>
                                    <label for="contact-message">Your message</label>
                                </div>
                            </div>
                            <!-- Grid column -->
                            <div class="text-center text-md-left">
                                <button type="submit" class="btn btn-info btn-md">Send</button>
                            </div>

                        </div>
                        <!-- Grid row -->

                    </form>

                </div>
                <!-- Grid column -->

                <!-- Grid column -->
                <div class="col-md-4 text-center">
                    <ul class="list-unstyled mb-0">
                        <li>
                            <i class="fas fa-map-marker-alt fa-2x text-default"></i>
                            <p>San Francisco, CA 94126, USA</p>
                        </li>
                        <li>
                            <i class="fas fa-phone fa-2x mt-4 text-default"></i>
                            <p>+ 01 234 567 89</p>
                        </li>
                        <li>
                            <i class="fas fa-envelope fa-2x mt-4 text-default"></i>
                            <p class="mb-0">contact@example.com</p>
                        </li>
                    </ul>
                </div>
                <!-- Grid column -->

            </div>
            <!-- Grid row -->
        </div>

    </section>

    @include('frontend.partials.footer')

    <!-- /Ends your project here-->
@endsection