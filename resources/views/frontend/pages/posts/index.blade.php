@extends('frontend.layouts.master')

@section('title')
    NeerBlog - Home
@endsection

@section('content')
    <!-- Start your project here-->
    <header>
        {{--uppernav--}}
        @include('frontend.partials.uppernav')

        {{--main nav--}}
        @include('frontend.partials.nav')
    </header>
    {{--slider--}}
    @include('frontend.partials.slider')

    {{--latest post--}}
    <section id="latest_post">
        <div class="container">
            <h2 class="mb-3">Latest Posts</h2>
            <hr>

            @foreach($posts as $post)
            <!-- Card -->
            <div class="card card-cascade wider reverse">

                <!-- Card image -->
                <div class="view view-cascade overlay">
                    <img class="card-img-top" src="{{ asset('images/post/'.$post->image) }}" alt="{{$post->title}}" height="400px">
                    <a href="{{ route('post.show',$post->slug) }}">
                        <div class="mask rgba-white-slight"></div>
                    </a>
                </div>

                <!-- Card content -->
                <div class="card-body card-body-cascade text-center">

                    <!-- Title -->
                    <a href="{{ route('post.show',$post->slug) }}"><h4 class="card-title"><strong>{{$post->title}}</strong></h4></a>
                    <a href="{{ route('post.categoryposts.show',$post->category->id) }}" class="pink-text">
                       <h6 class="font-weight-bold indigo-text py-2">{{$post->category->name}}</h6>
                    </a>
                    <!-- Text -->
                    <p class="card-text">{{ str_limit($post->body,200) }}</p>

                    <div class="row">
                        <div class="col-md-6">
                            <a class="btn btn-info btn-sm" href="{{ route('post.show',$post->slug) }}">Read more</a>
                        </div>
                        <div class="col-md-6">
                            <!-- Linkedin -->
                            <a href="https://www.linkedin.com/sharing/share-offsite/?url={{ urlencode(\Illuminate\Support\Facades\Request::fullUrl()).'/post/'. $post->slug }}" target="_blank" class="px-2 fa-lg li-ic"><i class="fab fa-linkedin-in"></i></a>
                            <!-- Twitter -->
                            <a href="https://twitter.com/intent/tweet?url={{ urlencode(\Illuminate\Support\Facades\Request::fullUrl()).'/post/'. $post->slug }}" target="_blank" class="px-2 fa-lg tw-ic"><i class="fab fa-twitter"></i></a>
                            <!-- Dribbble -->
                            <a href="http://www.facebook.com/share.php?url={{ urlencode(\Illuminate\Support\Facades\Request::fullUrl()).'/post/'. $post->slug }}" target="_blank" class="px-2 fa-lg fb-ic"><i class="fab fa-facebook-f"></i></a>
                        </div>
                    </div>

                </div>

            </div>
            <!-- Card -->
                <hr>
                @endforeach

        </div>

        <div style="margin-left: 50%">
            {{ $posts->links() }}
        </div>
    </section>
    {{--footer--}}
    @include('frontend.partials.footer')

    <!-- /Ends your project here-->
@endsection
@section('script')

@endsection