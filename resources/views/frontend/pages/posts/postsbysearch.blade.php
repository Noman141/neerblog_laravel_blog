@extends('frontend.layouts.master')

@section('title')
    NeerBlog - Home
@endsection

@section('content')
    <!-- Start your project here-->
    <header>
        {{--uppernav--}}
        @include('frontend.partials.uppernav')

        {{--main nav--}}
        @include('frontend.partials.nav')
    </header>

    {{--latest post--}}
    <section class="text-center" id="showByCategory">

        <div class="container">
            <!-- Section heading -->
            <h2 class="h1-responsive font-weight-bold my-5">All Post By Your Searching <strong>{{$search}}</strong></h2>


            <!-- Grid row -->
            <div class="row">
                {{--@php--}}
                    {{--$posts = $category->posts()->paginate(3);--}}
                {{--@endphp--}}

                @if($posts->count() > 0)
                    @foreach($posts as $post)

                        <div class="col-lg-4 col-md-6 mb-5">
                            <!-- Card -->
                            <div class="card card-cascade narrower">

                                <div class="view view view-cascade overlay rounded z-depth-2">
                                    <img class="img-fluid" src="{{ asset('images/post/'.$post->image) }}" alt="Sample image">
                                    <a>
                                        <a href="{{ route('post.show',$post->slug) }}" class="mask rgba-white-slight"></a>
                                    </a>
                                </div>

                                <!-- Card content -->
                                <div class="card-body card-body-cascade">

                                    <a href="{{ route('post.categoryposts.show',$post->category->id) }}" class="pink-text">
                                        <h6 class="font-weight-bold mb-3"><i class="fas fa-map pr-2"></i>{{ $post->category->name }}</h6>
                                    </a>
                                    <!-- Post title -->
                                    <a href="{{ route('post.show',$post->slug) }}">
                                        <h6 class="font-weight-bold mb-3"><strong>{{ str_limit($post->title,30) }}</strong></h6>
                                    </a>
                                    <!-- Post data -->
                                    <p>by <a class="font-weight-bold">{{ $post->admin->name }}</a>, {{ $post->created_at->format('jS F Y') }}</p>
                                    <!-- Excerpt -->
                                    <p class="dark-grey-text">{{ str_limit($post->body,150) }}</p>
                                    <!-- Read more button -->
                                    <a href="{{ route('post.show',$post->slug) }}" class="btn btn-pink btn-rounded btn-md">Read more</a>

                                </div>
                                <!-- Card content -->
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="alert alert-warning" style="margin-left: 25%">
                        <h1>No Posts In This Category</h1>
                    </div>
                @endif
            </div>
        </div>

        <div style="margin-left: 50%">
            {{ $posts->links() }}
        </div>
    </section>
    {{--footer--}}
    @include('frontend.partials.footer')

    <!-- /Ends your project here-->
@endsection