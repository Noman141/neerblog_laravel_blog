@extends('frontend.layouts.master')

@section('title')
    NeerShop - Posts
@endsection

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/showpost.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/owl.carousel.min.css') }}">
@endsection

@section('content')
    <!-- Start your project here-->
    <header>
        {{--uppernav--}}
        @include('frontend.partials.uppernav')

        {{--main nav--}}
        @include('frontend.partials.nav')
    </header>

    <!-- blog side -->
    <section class="blog-side sp-seven blog-style-one standard-post video-post" id="blogSide">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 content-side">
                    <div class="blog-details-content">
                        <div class="video-gallery">
                            <img src="{{ asset('images/post/'.$post->image) }}" alt="{{ $post->title }}">
                        </div>
                        <div class="blog-content-one sp-three">
                            <div class="top-content centred mt-3">
                                {{--<div class="meta-text">{{ $post->category->name }}</div>--}}
                                <a href="{{ route('post.categoryposts.show',$post->category->id) }}" class="pink-text">
                                    <h6 class="font-weight-bold py-2 meta-text">{{$post->category->name}}</h6>
                                </a>
                                <div class="date"><span>On</span> {{ $post->created_at->format('jS F Y') }} &nbsp;&nbsp;<i class="flaticon-circle"></i>&nbsp;&nbsp;<span>By</span> {{ $post->admin->name }}</div>
                            </div>
                            <div class="text">
                                {{ $post->body }}
                            </div>
                            <hr>

                            <p><strong>Tags :
                                        @foreach((array)$post->tags as $tag)
                                        <span class="badge badge-info m-2 p-2">{{ $tag }}</span>
                                        @endforeach
                                    </strong></p>
                            <ul class="meta-list centred">
                                <li>
                                    <a href="#"><i class="fa fa-comments-o" aria-hidden="true"></i>&nbsp; {{ App\Models\Comment::orderBy('id','desc')->where('post_id',$post->id)->where('status',1)->count() }} Comments</a>
                                </li>

                                <li>
                                    <a href="#comment"><i class="fa fa-share-alt" aria-hidden="true"></i> &nbsp;Comment</a>
                                </li>

                                <li>
                                    <a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i> &nbsp;Share</a>
                                </li>
                            </ul>
                        </div>

                        <div class="related-post centred">
                            <div class="title-text-two">RELATED POSTS</div>
                            <!-- Section: Testimonials v.4 -->
                            <div class="text-center my-5" id="related_post">
                                <div id="carousel-example-multi" class="carousel slide carousel-multi-item v-2" data-ride="carousel">
                                    <!--Controls-->
                                    <div class="controls-top">
                                        <a class="btn-floating" href="#carousel-example-multi" data-slide="prev"><i class="fas fa-chevron-left"></i></a>
                                        <a class="btn-floating" href="#carousel-example-multi" data-slide="next"><i class="fas fa-chevron-right"></i></a>
                                    </div>
                                    <!--/.Controls-->
                                    <div class="carousel-inner " role="listbox">
                                        @foreach(App\Models\Post::where('category_id',$post->category_id)->get() as $relatedpost)
                                        <div class="carousel-item {{ $loop->index == 0?'active':'' }}">
                                            <div class="col-12 col-md-4">
                                                <div class="card mb-2">
                                                    <a href="{{ route('post.show',$post->slug) }}">
                                                       <img class="card-img-top" src="{{ asset('images/post/'.$relatedpost->image) }}" alt="Card image cap">
                                                    </a>
                                                    <div class="card-body">
                                                        <a href="{{ route('post.show',$post->slug) }}">
                                                          <h4 class="card-title font-weight-bold">{{ str_limit($relatedpost->title,30) }}</h4>
                                                        </a>
                                                        <p class="card-text">{{ str_limit($relatedpost->body,150) }}</p>
                                                        <a class="btn btn-primary btn-md btn-rounded" href="{{ route('post.show',$post->slug) }}">read more</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                            @endforeach
                                    </div>

                                </div>
                            </div>
                            <!-- Section: Testimonials v.4 -->
                        </div>
                    </div>
                    <div class="comment-area" id="comment">
                        <div class="title-text-two">{{ App\Models\Comment::orderBy('id','desc')->where('post_id',$post->id)->where('status',1)->count() }} COMMENTS</div>
                        @foreach(App\Models\Comment::orderBy('id','desc')->where('post_id',$post->id)->where('status',1)->get() as $comment)
                            <div class="single-comment">
                                <div class="img-box"><figure><img src="images/news/c1.jpg" alt=""></figure></div>
                                <div class="cyan-text">{{ $comment->name }}</div>
                                <div class="comment-time">{{ $comment->created_at->diffForHumans() }}</div>
                                <div class="text">
                                    {{ $comment->comment }}
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="comment-form">
                        <div class="title-text-two">WRITE YOUR COMMENTS</div>
                        <form action="{{ route('comment.store') }}" method="post">
                            @csrf
                            <div class="row">
                                <input type="hidden" name="post_id" value="{{ $post->id }}">
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <textarea placeholder="Enter your comments here..." name="comment" required=""></textarea>
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="name" value="" placeholder="Name" required="">
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <input type="email" name="email" value="" placeholder="Email" required="">
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <button type="submit" class="btn-one">POST COMMENT</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- blog side end -->
    {{--footer--}}
    @include('frontend.partials.footer')

    <!-- /Ends your project here-->
@endsection
@section('script')
    <script type="text/javascript" src="{{ asset('js/frontend/owl.carousel.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $(".owl-carousel").owlCarousel();
        });
    </script>
@endsection