<!-- Subscribe form starts -->
<div class="modal fade" id="modalSubscriptionForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-notify modal-dialog-centered cascading-modal" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header text-center">
                <h4 class="modal-title white-text w-100 font-weight-bold py-2">Subscribe</h4>
                <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="white-text">&times;</span>
                </button>
            </div>
            <form action="{{ route('subscriber.store') }}" method="post">
                @csrf
                <!--Body-->
                    @include('global.msg')
                <div class="modal-body">

                    <div class="md-form mb-5">
                        <i class="fa fa-user prefix grey-text"></i>
                        <input type="text" id="form3" class="form-control validate" name="name" >
                        <label data-error="wrong" data-success="right" for="form3">Your name</label>
                    </div>

                    <div class="md-form">
                        <i class="fa fa-envelope prefix grey-text"></i>
                        <input type="email" id="form2" class="form-control validate" name="email" >
                        <label data-error="wrong" data-success="right" for="form2">Your email</label>
                    </div>
                </div>

                <span class="text-center text-info">When You Subscribe You Will get All Of Our Updates</span>

                <!--Footer-->
                <div class="modal-footer justify-content-center">
                    <button type="submit" class="btn btn-outline-info waves-effect">Subscribe <i class="fa fa-paper-plane-o ml-1"></i></button>
                </div>
            </form>
        </div>
        <!--/.Content-->
    </div>
</div>
<!-- subscribe form ends -->
</form>