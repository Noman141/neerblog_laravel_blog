<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
<!-- Bootstrap core CSS -->
<link href="{{ asset('css/frontend/bootstrap.min.css') }}" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="{{ asset('css/frontend/mdb.min.css') }}" rel="stylesheet">

<!-- Your custom styles (optional) -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/main.css') }}">

@yield('style')