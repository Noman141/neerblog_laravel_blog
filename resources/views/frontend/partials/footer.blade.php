<!-- Footer -->
<footer class="page-footer font-small default-color-dark pt-4">

    <!-- Footer Links -->
    <div class="container text-center text-md-left">

        <!-- Grid row -->
        <div class="row">

            <!-- Grid column -->
            <div class="col-md-4 mx-auto">

                <!-- Content -->
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Footer Content</h5>
                <p>Here you can use rows and columns here to organize your footer content. Lorem ipsum dolor sit amet, consectetur
                    adipisicing elit.</p>

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none">

            <!-- Grid column -->
            <div class="col-md-4 mx-auto">

                <!-- Links -->
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4">All Categories</h5>

                <ul class="list-unstyled">
                    @foreach(App\Models\Category::orderBy('name','asc')->where('parent_id',NULL)->get() as $parent_category)
                    <li>
                        <a href="{{route('post.parentcategoryposts.show',$parent_category->id) }}">{{ $parent_category->name }}</a>
                    </li>
                    @endforeach
                </ul>

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none">

            <!-- Grid column -->
            <div class="col-md-4 mx-auto">

                <!-- Links -->
                <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Pages</h5>

                <ul class="list-unstyled">
                    <li>
                        <a href="#!">Home </a>
                    </li>
                    <li>
                        <a href="#!">About</a>
                    </li>
                    <li>
                        <a href="#!">Contact</a>
                    </li>
                    <li>
                        <a href="#!">About Us</a>
                    </li>
                    <li>
                        <a href="#!">Terms And Conditions </a>
                    </li>
                </ul>

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none">

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Links -->

    <hr>
    <!-- Call to action -->
    <ul class="list-unstyled list-inline text-center py-2">
        <li class="list-inline-item">
            <h5 class="mb-1">Subscribe Us</h5>
        </li>
        <li class="list-inline-item">
            <a href="" class="btn btn-info btn-rounded" data-toggle="modal" data-target="#modalSubscriptionForm">Subscribe</a>
        </li>
    </ul>
    <!-- Call to action -->

    @include('frontend.pages.subscribe.index')

    <hr>

@php
    $links =App\Models\SocalMedia::first();
@endphp

    <!-- Social buttons -->
    <ul class="list-unstyled list-inline text-center">
        <li class="list-inline-item">
            <a target="_blank" href="{{ $links->facebook_link }}" class="btn-floating btn-fb mx-1">
                <i class="fab fa-facebook-f"> </i>
            </a>
        </li>
        <li class="list-inline-item">
            <a target="_blank" href="{{ $links->twitter_link }}" class="btn-floating btn-tw mx-1">
                <i class="fab fa-twitter"> </i>
            </a>
        </li>
        <li class="list-inline-item">
            <a class="btn-floating btn-gplus mx-1">
                <i class="fab fa-google-plus-g"> </i>
            </a>
        </li>
        <li class="list-inline-item">
            <a target="_blank" href="{{ $links->linkedin_link }}" class="btn-floating btn-li mx-1">
                <i class="fab fa-linkedin-in"> </i>
            </a>
        </li>
        <li class="list-inline-item">
            <a target="_blank" href="{{ $links->dribbble_link }}" class="btn-floating btn-dribbble mx-1">
                <i class="fab fa-dribbble"> </i>
            </a>
        </li>
    </ul>
    <!-- Social buttons -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© 2018 Copyright:
        <a href="#"> NeerBlog</a>
    </div>
    <!-- Copyright -->

</footer>
<!-- Footer -->