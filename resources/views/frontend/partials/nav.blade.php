<!--Navbar -->
<nav class=" navbar navbar-expand-lg navbar-dark default-color m-0 main-nav" id="main_nav">
    <div class="container">
        <a class="navbar-brand" href="{{ route('index') }}" id="logo"><img src="{{ asset('img/frontend/noman.png') }}"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555"
                aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent-555">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ route('index') }}">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <!-- Dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Category</a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @foreach(App\Models\Category::orderBy('id','asc')->where('parent_id',NULL)->get() as $parent)
                            <li class="dropdown">
                                <a class="dropdown-toggle" href="{{ route('post.categoryposts.show',$parent->id) }}" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ $parent->name }}</a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    @foreach(App\Models\Category::orderBy('id','asc')->where('parent_id',$parent->id)->get() as $category_child)
                                        <a class="dropdown-item" href="{{ route('post.categoryposts.show',$category_child->id) }}">{{ $category_child->name }} </a>
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('about') }}">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('contact') }}">Contact</a>
                </li>

            </ul>


            <form id="search" action="{{ route('search') }}" method="get">
                <div class="md-form my-0">
                    <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search" name="search">
                </div>
            </form>

            <ul class="navbar-nav ml-auto nav-flex-icons">
                <li class="nav-item avatar">
                    <a class="nav-link p-0" href="#">
                        <img src="https://mdbootstrap.com/img/Photos/Avatars/avatar-5.jpg" class="rounded-circle z-depth-0" alt="avatar image" height="35">
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!--/.Navbar -->