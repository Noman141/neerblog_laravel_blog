<section id="slider">
    <!--Carousel Wrapper-->
    <div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
        <!--Indicators-->
        <ol class="carousel-indicators">
            @foreach($sliders as $slider)
                <li data-target="#carousel-example-2" data-slide-to="{{$loop->index}}" class="{{ $loop->index == 0?'active':'' }}"></li>
            @endforeach
        </ol>
        <!--/.Indicators-->
        <!--Slides-->
        <div class="carousel-inner" role="listbox">
            @foreach($sliders as $slider)
            <div class="carousel-item {{ $loop->index == 0?'active':'' }}">
                <div class="view">
                    <img class="d-block w-100" src="{{ asset('images/slider/'.$slider->image) }}" alt="First slide">
                    <div class="mask rgba-black-light"></div>
                </div>
                <div class="carousel-caption">
                    <h1 class="h3-responsive">{{ $slider->post_title }}</h1>
                    <p>{{ str_limit($slider->post_body,150) }}</p>
                    <a href="{{ $slider->btn_link }}" class="btn btn-info btn-sm">{{ $slider->btn_text }}</a>
                </div>
            </div>
            @endforeach
        </div>
        <!--/.Slides-->
        <!--Controls-->
        <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->
</section>