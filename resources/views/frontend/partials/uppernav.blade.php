<div class="container" id="uppernav">
    <ul class="d-flex justify-content-center">
        <li class="entry-share">Like Us  </li>
       @php
           $links =App\Models\SocalMedia::first();
       @endphp
        <li class="">
            <a class="fb-ic" target="_blank" href="{{ $links->facebook_link }}"><i class="fab fa-facebook-f"></i></a>
        </li>
        <li class="">
            <a class="li-ic" target="_blank" href="{{ $links->linkedin_link }}"><i class="fab fa-linkedin-in"></i></a>
        </li>
        <li class="">
            <a class="tw-ic" target="_blank" href="{{ $links->twitter_link}}"><i class="fab fa-twitter"></i></a>
        </li>
        <li class="">
            <a class="db-ic" target="_blank" href="{{ $links->instagram_link }}"><i class="fab fa-dribbble"></i></a>
        </li>
        <li class="">
            <a class="ins-ic" target="_blank" href="{{ $links->dribbble_link }}"><i class="fab fa-instagram"></i></a>
        </li>
    </ul>

</div>