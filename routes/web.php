<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/***************************************************************************
Frontend routes Start from here
 ***************************************************************************/

//HomePage controller
Route::get('/', 'Frontend\PagesController@index')->name('index');
//subscribers store route
Route::post('/subscribe', 'Frontend\SubscriberController@store')->name('subscriber.store');


//Contact Page route
Route::group(['prefix' => 'contact'], function() {

    Route::get('/','Frontend\ContactController@index')->name('contact');
//    send message route
    Route::post('/store','Frontend\ContactController@store')->name('contact.store');

});

//About Page route
Route::group(['prefix' => 'about'], function() {

    Route::get('/us','Frontend\AboutController@index')->name('about');
    Route::get('/{slug}','Frontend\AboutController@show')->name('about.show');

});

//Post route
Route::group(['prefix' => 'post'], function() {

    Route::get('/','Frontend\PostController@index')->name('post.index');
//  posts show route
    Route::get('/{slug}','Frontend\PostController@show')->name('post.show');
//    posts by category route
    Route::get('/category/{id}','Frontend\PostController@postByCategory')->name('post.categoryposts.show');
//    posts by category route
    Route::get('/parent/category/{id}','Frontend\PagesController@showingPostsByParentCategory')->name('post.parentcategoryposts.show');
//    search route
    Route::get('/posts/search','Frontend\PagesController@search')->name('search');
//    Comment store route
    Route::post('/store', 'Frontend\CommentController@store')->name('comment.store');

});
/***************************************************************************
Frontend routes Ends here
 ***************************************************************************/

/***************************************************************************
Backed routes Start from here
 ***************************************************************************/
Route::group(['prefix' => '/admin'],function() {
//    admin dashbard route
    Route::get('/','Backend\AdminPagesController@index')->name('admin.dashboard');
//Admin Login Route
    Route::get('/login','Auth\Admin\LoginController@showLoginForm')->name('admin.login');

    Route::post('/login/submit','Auth\Admin\LoginController@login')->name('admin.login.submit');
//    admin logout route
    Route::post('/logout','Auth\Admin\LoginController@logout')->name('admin.logout');
//    Admin list route
    Route::get('/list','Backend\AdminController@index')->name('admin.list');
// Admin show route
    Route::get('/show/{id}','Backend\AdminController@show')->name('admin.show');
//    Admin edit route
    Route::get('/edit/{id}','Backend\AdminController@edit')->name('admin.edit');
//    Admin update route
    Route::post('/update/{id}','Backend\AdminController@update')->name('admin.update');
//    admin block route
    Route::get('/block/{id}','Backend\AdminController@block')->name('admin.block');
//    admin ulblock route
    Route::get('/unblock/{id}','Backend\AdminController@unblock')->name('admin.unblock');
// delete admin route
    Route::get('/user/{id}','Backend\AdminController@delete')->name('admin.delete');

//    admin email verify  route
    Route::get('/token/{token}','Backend\VerifyController@verify')->name('admin.registration.verify');
//    admin register-form route
    Route::get('/register/form','Auth\Admin\RegisterController@showRegistrationForm')->name('admin.register.form');
//   admin register route
    Route::post('/register','Auth\Admin\RegisterController@register')->name('admin.register');
//    admin password reset route
    Route::post('/password/email','Auth\Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');

    Route::get('/password/reset','Auth\Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');

    Route::post('/password/reset','Auth\Admin\ResetPasswordController@reset')->name('admin.password.update');

    Route::get('/password/reset/{token}','Auth\Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');

    //admin Category route starts

    Route::group(['prefix' => 'category'],function() {

        Route::get('/','Backend\CategoryController@index')->name('admin.category');

        Route::get('/create','Backend\CategoryController@create')->name('admin.category.create');

        Route::post('/store','Backend\CategoryController@store')->name('admin.category.store');

        Route::get('/edit/{id}','Backend\CategoryController@edit')->name('admin.category.edit');

        Route::post('/update/{id}','Backend\CategoryController@update')->name('admin.category.update');

        Route::get('/delete/{id}','Backend\CategoryController@delete')->name('admin.category.delete');
    });
//admin Category route ends

    //admin Posts route starts

    Route::group(['prefix' => 'post'],function() {

        Route::get('/','Backend\PostController@index')->name('admin.post');

        Route::get('/create','Backend\PostController@create')->name('admin.post.create');

        Route::post('/store','Backend\PostController@store')->name('admin.post.store');

        Route::get('/{slug}','Backend\PostController@show')->name('admin.post.show');

        Route::get('/edit/{id}','Backend\PostController@edit')->name('admin.post.edit');

        Route::get('/publish/{id}','Backend\PostController@publish')->name('admin.post.publish');

        Route::post('/update/{id}','Backend\PostController@update')->name('admin.post.update');

        Route::get('/delete/{id}','Backend\PostController@delete')->name('admin.post.delete');
    });
//admin posts route ends

//admin Slider route starts

    Route::group(['prefix' => 'slider'],function() {

        Route::get('/list','Backend\SliderController@index')->name('admin.slider');

        Route::get('/create','Backend\SliderController@create')->name('admin.slider.create');

        Route::post('/store','Backend\SliderController@store')->name('admin.slider.store');

//        Route::get('/{slug}','Backend\SliderController@show')->name('admin.slider.show');

        Route::get('/edit/{id}','Backend\SliderController@edit')->name('admin.slider.edit');

        Route::post('/update/{id}','Backend\SliderController@update')->name('admin.slider.update');

        Route::get('/delete/{id}','Backend\SliderController@delete')->name('admin.slider.delete');
    });
//admin Slider route ends

//admin subscriber route starts

    Route::group(['prefix' => 'subscribers'],function() {
//        Show Subscribers
        Route::get('/list','Backend\SubscriberController@index')->name('admin.subscribers');

        Route::get('/approve/{id}','Backend\SubscriberController@approve')->name('admin.subscriber.approve');
//        Delete Subscribers
        Route::get('/delete/{id}','Backend\SubscriberController@delete')->name('admin.subscriber.delete');
    });
//   admin subscriber route ends

//   admin Comments route starts

    Route::group(['prefix' => 'comments'],function() {
//        Show comments
        Route::get('/list','Backend\CommentController@index')->name('admin.comments');

        Route::get('/show/{id}','Backend\CommentController@show')->name('admin.comment.show');

        Route::get('/publish/{id}','Backend\CommentController@publish')->name('admin.comment.publish');
//        Delete comments
        Route::get('/delete/{id}','Backend\CommentController@delete')->name('admin.comment.delete');
    });
//admin comments route ends

//   admin Socal Media Link route starts

    Route::group(['prefix' => 'socal-media-link'],function() {
//        Show comments
        Route::get('/list','Backend\SocalMediaController@index')->name('admin.socalmeidalinks');

        Route::get('/edit/{id}','Backend\SocalMediaController@edit')->name('admin.socalmeidalink.edit');

        Route::post('/update/{id}','Backend\SocalMediaController@update')->name('admin.socalmeidalink.update');

    });
//admin Socal Media Link route ends

//   admin Contact route starts

    Route::group(['prefix' => 'messages'],function() {
//        Show comments
        Route::get('/list','Backend\ContactController@index')->name('admin.messages');

        Route::get('/show/{id}','Backend\ContactController@show')->name('admin.message.show');

        Route::get('/reply/{id}','Backend\ContactController@replyForm')->name('admin.message.replyform');

        Route::post('/reply/{id}','Backend\ContactController@reply')->name('admin.message.reply');

        Route::get('/delete/{id}','Backend\ContactController@delete')->name('admin.message.delete');


    });
//admin Contact route ends
});




/***************************************************************************
Backed routes ends here
 ***************************************************************************/
//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
